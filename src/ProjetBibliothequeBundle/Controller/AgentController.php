<?php

namespace ProjetBibliothequeBundle\Controller;

use ProjetBibliothequeBundle\Entity\Archive;
use ProjetBibliothequeBundle\Entity\Emprunt;
use ProjetBibliothequeBundle\Entity\Inscrit;
use ProjetBibliothequeBundle\Form\EmpruntType;
use ProjetBibliothequeBundle\Form\ExemplaireType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use ProjetBibliothequeBundle\Entity\Livre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class AgentController extends Controller
{
    public function indexAction()
    {
        return $this->render('ProjetBibliothequeBundle:Agent:index.html.twig');
    }

    public function archiveAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('ProjetBibliothequeBundle:Archive');
        $nbArchive = $repository->findByCountArchive();
        $listeArchives = $repository->findAll();
        return $this->render('ProjetBibliothequeBundle:Agent:archive.html.twig', array('nbArchive' => $nbArchive, 'listeArchives' => $listeArchives));
    }

    public function enCoursAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('ProjetBibliothequeBundle:Emprunt');
        $listeEnCours = $repository->getAllInfoEmprunt();
        return $this->render('ProjetBibliothequeBundle:Agent:enCours.html.twig', array('listeEnCours' => $listeEnCours));
    }

    public function nonRenduAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('ProjetBibliothequeBundle:Emprunt');
        $maDate = date("Y-m-d");
        $maDate = date("Y-m-d", strtotime($maDate . "- 15 days"));
        $listeNonRendu = $repository->getEmpruntRetard($maDate);
        return $this->render('ProjetBibliothequeBundle:Agent:nonRendu.html.twig', array('listeNonRendu' => $listeNonRendu));
    }

    public function dispoAction(Request $request)
    {
        $livre=new Livre();
        $form=$this->createFormBuilder($livre)
            ->add('titre',TextType::class)
            ->add('Rechercher',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if($form->isValid()){
            $entityManager=$this->getDoctrine()->getManager();
            $repoLivre=$entityManager->getRepository('ProjetBibliothequeBundle:Livre');
            $repoExemplaire=$entityManager->getRepository('ProjetBibliothequeBundle:Exemplaire');
            $tmp=$livre->getTitre();
            $livres=$repoLivre->listerLivres($tmp);
            for($i=0;$i<sizeof($livres);$i++){
                $nbNonDispo=$repoExemplaire->nonDispoExemplaire($livres[$i]['id']);
                $livres[$i]['nbExemplaire']=$livres[$i]['nbExemplaire']-$nbNonDispo;
            }
            /*
            $entityManager=$this->getDoctrine()->getManager();
            $repository=$entityManager->getRepository('ProjetBibliothequeBundle:Livre');
            $tmp=$livre->getTitre();
            $livres=$repository->listerLivres($tmp);
            $repository=$entityManager->getRepository('ProjetBibliothequeBundle:Exemplaire');
            foreach ($livres as $unLivre){
                $nbDispo=$unLivre['nbExemplaire']-$repository->countByIdExemplaire($unLivre['id']);
                $unLivre['nbExemplaire']=$nbDispo;
            }*/
            return $this->render('ProjetBibliothequeBundle:Agent:resultat.html.twig', array('livres' => $livres));

        }
        return $this->render('ProjetBibliothequeBundle:Agent:dispo.html.twig',array('form'=>$form->createView()));

    }

    public function sortieLivreAction(Request $request){
        $livre=new Livre();
        $form=$this->createFormBuilder($livre)
            ->add('titre',TextType::class)
            ->add('Rechercher',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if($form->isValid()){
            $tmp=$livre->getTitre();
            $repository=$this->getDoctrine()->getManager()->getRepository('ProjetBibliothequeBundle:Livre');
            $livres=$repository->listerLivres($tmp);
            return $this->render('ProjetBibliothequeBundle:Agent:sortieLivre.html.twig', array('livres' => $livres));
        }
        return $this->render('ProjetBibliothequeBundle:Agent:choixLivre.html.twig',array('form'=>$form->createView()));
    }

    public function sortieInfoAction(Request $request, $idLivre){
        $emprunt=new Emprunt();
        $form=$this->createForm(EmpruntType::class,$emprunt);
        $form->handleRequest($request);
        if($form->isValid()){
            $entityManager=$this->getDoctrine()->getManager();
            if($emprunt->getEmpruntsInscrit()->getCycle()==1){
                $repoEmprunt=$entityManager->getRepository('ProjetBibliothequeBundle:Emprunt');
                $dispo=$repoEmprunt->empruntDispo($emprunt->getEmpruntsInscrit()->getId());
                if ($dispo >= 5){
                    echo '<div class="alert alert-danger" role="alert">Vous avez atteint la limite de vos prêts!</div>';
                    return $this->render('ProjetBibliothequeBundle:Agent:choixInfo.html.twig',array('idLivre'=>$idLivre,'form'=>$form->createView()));
                }
            }
            $repoExemplaire=$entityManager->getRepository('ProjetBibliothequeBundle:Exemplaire');
            $exemplaire=$repoExemplaire->findOneBy(array("exemplairesLivre"=>$idLivre,"dispo"=>0));
            $exemplaire->setDispo(1);
            $entityManager->flush();
            $emprunt->setEmpruntsExemplaire($exemplaire);
            $entityManager->persist($emprunt);
            $entityManager->flush();
            echo '<div class="alert alert-success" role="alert">Votre prêt a bien été enregistré!</div>';
            return $this->render('ProjetBibliothequeBundle:Agent:index.html.twig');
        }
        return $this->render('ProjetBibliothequeBundle:Agent:choixInfo.html.twig',array('idLivre'=>$idLivre,'form'=>$form->createView()));
    }

    public function rentreeInscritAction(Request $request){
        $inscrit=new Inscrit();
        $form=$this->createFormBuilder($inscrit)
            ->add('nom',TextType::class)
            ->add('Rechercher',SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if($form->isValid()){
            $tmp=$inscrit->getNom();
            $repository=$this->getDoctrine()->getManager()->getRepository('ProjetBibliothequeBundle:Inscrit');
            $inscrits=$repository->listerInscrits($tmp);
            return $this->render('ProjetBibliothequeBundle:Agent:rentreeEmprunt.html.twig', array('inscrits' => $inscrits));
        }
        return $this->render('ProjetBibliothequeBundle:Agent:rentreeInscrit.html.twig',array('form'=>$form->createView()));
    }

    public function rentreeEmpruntAction($idInscrit){
        $repository=$this->getDoctrine()->getManager()->getRepository('ProjetBibliothequeBundle:Emprunt');
        $emprunts=$repository->listerEmprunts($idInscrit);
        return $this->render('ProjetBibliothequeBundle:Agent:rentreeArchive.html.twig', array('idInscrit'=>$idInscrit,'emprunts' => $emprunts));
    }

    public function rentreeArchiveAction($idInscrit, $idEmprunt){
        $entityManager=$this->getDoctrine()->getManager();
        $repoEmprunt=$entityManager->getRepository('ProjetBibliothequeBundle:Emprunt');
        $repoExemplaire=$entityManager->getRepository('ProjetBibliothequeBundle:Exemplaire');
        $repoInscrit=$entityManager->getRepository('ProjetBibliothequeBundle:Inscrit');
        $emprunt=$repoEmprunt->find($idEmprunt);
        $exemplaire=$repoExemplaire->findOneBy(array('id'=>$emprunt->getEmpruntsExemplaire()));
        $tmp=$repoInscrit->find($idInscrit);
        $tmp2=$repoEmprunt->titreEmprunt($idEmprunt);
        $archive = new Archive();
        $archive->setTitreLivre($tmp2['titre']);
        $archive->setNomPrenom($tmp->getNom().'-'.$tmp->getPrenom());
        $archive->setDateDebut(date_format($emprunt->getDateEmprunt(),'Y-m-d'));
        $archive->setDateFin(date('Y-m-d'));
        $exemplaire->setDispo(0);
        $entityManager->flush();
        $entityManager->remove($emprunt);
        $entityManager->flush();
        $entityManager->persist($archive);
        $entityManager->flush();
        echo '<div class="alert alert-success" role="alert">Le retour du livre a bien été enregistré!</div>';
        return $this->render('ProjetBibliothequeBundle:Agent:index.html.twig');
    }
    
}
?>
<?php

namespace ProjetBibliothequeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ProjetBibliothequeBundle\Entity\Rayon;
use ProjetBibliothequeBundle\Form\RayonType;

/**
 * Rayon controller.
 *
 */
class RayonController extends Controller
{
    /**
     * Lists all Rayon entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rayons = $em->getRepository('ProjetBibliothequeBundle:Rayon')->findAll();

        return $this->render('ProjetBibliothequeBundle:Rayon:index.html.twig', array(
            'rayons' => $rayons,
        ));

        // le return a changer par : return $this->render('ProjetBibliothequeBundle:Rayon:index.html.twig')
    }

    /**
     * Creates a new Rayon entity.
     *
     */
    public function newAction(Request $request)
    {
        $rayon = new Rayon();
        $form = $this->createForm('ProjetBibliothequeBundle\Form\RayonType', $rayon);
        // remplacer la ligne dessus par $this->createForm(Rayon::class, $rayon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rayon);
            $em->flush();

            return $this->redirectToRoute('rayon_show', array('id' => $rayon->getId()));
        }

        return $this->render('rayon/new.html.twig', array(
            'rayon' => $rayon,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Rayon entity.
     *
     */
    public function showAction(Rayon $rayon)
    {
        $deleteForm = $this->createDeleteForm($rayon);

        /*return $this->render('rayon/show.html.twig', array(
            'rayon' => $rayon,
            'delete_form' => $deleteForm->createView(),
        ));*/

        return $this->render('ProjetBibliothequeBundle:Rayon:show.html.twig', array(
            'rayon' => $rayon,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Rayon entity.
     *
     */
    public function editAction(Request $request, Rayon $rayon)
    {
        $deleteForm = $this->createDeleteForm($rayon);
        $editForm = $this->createForm('ProjetBibliothequeBundle\Form\RayonType', $rayon);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rayon);
            $em->flush();

            return $this->redirectToRoute('rayon_edit', array('id' => $rayon->getId()));
        }

        return $this->render('rayon/edit.html.twig', array(
            'rayon' => $rayon,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Rayon entity.
     *
     */
    public function deleteAction(Request $request, Rayon $rayon)
    {
        $form = $this->createDeleteForm($rayon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($rayon);
            $em->flush();
        }

        return $this->redirectToRoute('rayon_index');
    }

    /**
     * Creates a form to delete a Rayon entity.
     *
     * @param Rayon $rayon The Rayon entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Rayon $rayon)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('rayon_delete', array('id' => $rayon->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

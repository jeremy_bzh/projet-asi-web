<?php

namespace ProjetBibliothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ConservateurController extends Controller
{
    public function indexAction()
    {
        return $this->render('ProjetBibliothequeBundle:Conservateur:index.html.twig');
    }

    public function afficheLivreAction()
    {
        return $this->render('ProjetBibliothequeBundle:Conservateur:livres.html.twig');
    }
}

?>
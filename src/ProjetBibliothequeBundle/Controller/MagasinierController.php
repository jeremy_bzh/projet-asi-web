<?php

namespace ProjetBibliothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MagasinierController extends Controller
{
    public function indexAction()
    {
        return $this->render('ProjetBibliothequeBundle:Magasinier:index.html.twig');
    }

    public function listeExemplairesAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('ProjetBibliothequeBundle:Exemplaire');

        $nbLivres = $repository->findByCountLivres();
        $listeLivres = $repository->findAll();

        return $this->render('ProjetBibliothequeBundle:Magasinier:livres.html.twig', array('nbLivres'=>$nbLivres,'listeLivres'=>$listeLivres));
    }

    public function listeRayonsAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('ProjetBibliothequeBundle:Rayon');

        $nbRayons = $repository->findByCountRayons();
        $listeRayons = $repository->findAll();

        return $this->render('ProjetBibliothequeBundle:Magasinier:rayons.html.twig', array('nbRayons'=>$nbRayons,'listeRayons'=>$listeRayons));
    }

    public function ajoutRayonAction()
    {
        echo "lol";
        return new Response("lol");
    }


    public function listeEtageresAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('ProjetBibliothequeBundle:Etagere');

        $nbEtageres = $repository->findByCountEtageres();
        $listeEtageres = $repository->findAll();

        return $this->render('ProjetBibliothequeBundle:Magasinier:etageres.html.twig', array('nbEtageres'=>$nbEtageres,'listeEtageres'=>$listeEtageres));
    }

    public function ajoutEtagereAction()
    {
        echo "lol";
        return new Response("lol");
    }

}

?>
<?php

namespace ProjetBibliothequeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ProjetBibliothequeBundle:Default:index.html.twig');
    }
}

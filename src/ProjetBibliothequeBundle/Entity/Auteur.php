<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Auteur
 */
class Auteur
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $auteursLivres;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->auteursLivres = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Auteur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Auteur
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Add auteursLivre
     *
     * @param \ProjetBibliothequeBundle\Entity\Livre $auteursLivre
     *
     * @return Auteur
     */
    public function addAuteursLivre(\ProjetBibliothequeBundle\Entity\Livre $auteursLivre)
    {
        $this->auteursLivres[] = $auteursLivre;

        return $this;
    }

    /**
     * Remove auteursLivre
     *
     * @param \ProjetBibliothequeBundle\Entity\Livre $auteursLivre
     */
    public function removeAuteursLivre(\ProjetBibliothequeBundle\Entity\Livre $auteursLivre)
    {
        $this->auteursLivres->removeElement($auteursLivre);
    }

    /**
     * Get auteursLivres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuteursLivres()
    {
        return $this->auteursLivres;
    }
}

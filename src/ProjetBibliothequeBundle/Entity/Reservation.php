<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Reservation
 */
class Reservation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateReservation;

    /**
     * @var \ProjetBibliothequeBundle\Entity\Exemplaire
     */
    private $reservationsExemplaire;

    /**
     * @var \ProjetBibliothequeBundle\Entity\Inscrit
     */
    private $reservationsInscrit;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateReservation
     *
     * @param \DateTime $dateReservation
     *
     * @return Reservation
     */
    public function setDateReservation($dateReservation)
    {
        $this->dateReservation = $dateReservation;

        return $this;
    }

    /**
     * Get dateReservation
     *
     * @return \DateTime
     */
    public function getDateReservation()
    {
        return $this->dateReservation;
    }

    /**
     * Set reservationsExemplaire
     *
     * @param \ProjetBibliothequeBundle\Entity\Exemplaire $reservationsExemplaire
     *
     * @return Reservation
     */
    public function setReservationsExemplaire(\ProjetBibliothequeBundle\Entity\Exemplaire $reservationsExemplaire = null)
    {
        $this->reservationsExemplaire = $reservationsExemplaire;

        return $this;
    }

    /**
     * Get reservationsExemplaire
     *
     * @return \ProjetBibliothequeBundle\Entity\Exemplaire
     */
    public function getReservationsExemplaire()
    {
        return $this->reservationsExemplaire;
    }

    /**
     * Set reservationsInscrit
     *
     * @param \ProjetBibliothequeBundle\Entity\Inscrit $reservationsInscrit
     *
     * @return Reservation
     */
    public function setReservationsInscrit(\ProjetBibliothequeBundle\Entity\Inscrit $reservationsInscrit = null)
    {
        $this->reservationsInscrit = $reservationsInscrit;

        return $this;
    }

    /**
     * Get reservationsInscrit
     *
     * @return \ProjetBibliothequeBundle\Entity\Inscrit
     */
    public function getReservationsInscrit()
    {
        return $this->reservationsInscrit;
    }
}

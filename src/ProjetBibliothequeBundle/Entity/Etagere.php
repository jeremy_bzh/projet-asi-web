<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Etagere
 */
class Etagere
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $numero;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $etagereExemplaires;

    /**
     * @var \ProjetBibliothequeBundle\Entity\Rayon
     */
    private $etageresRayon;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->etagereExemplaires = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Etagere
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Add etagereExemplaire
     *
     * @param \ProjetBibliothequeBundle\Entity\Exemplaire $etagereExemplaire
     *
     * @return Etagere
     */
    public function addEtagereExemplaire(\ProjetBibliothequeBundle\Entity\Exemplaire $etagereExemplaire)
    {
        $this->etagereExemplaires[] = $etagereExemplaire;

        return $this;
    }

    /**
     * Remove etagereExemplaire
     *
     * @param \ProjetBibliothequeBundle\Entity\Exemplaire $etagereExemplaire
     */
    public function removeEtagereExemplaire(\ProjetBibliothequeBundle\Entity\Exemplaire $etagereExemplaire)
    {
        $this->etagereExemplaires->removeElement($etagereExemplaire);
    }

    /**
     * Get etagereExemplaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtagereExemplaires()
    {
        return $this->etagereExemplaires;
    }

    /**
     * Set etageresRayon
     *
     * @param \ProjetBibliothequeBundle\Entity\Rayon $etageresRayon
     *
     * @return Etagere
     */
    public function setEtageresRayon(\ProjetBibliothequeBundle\Entity\Rayon $etageresRayon = null)
    {
        $this->etageresRayon = $etageresRayon;

        return $this;
    }

    /**
     * Get etageresRayon
     *
     * @return \ProjetBibliothequeBundle\Entity\Rayon
     */
    public function getEtageresRayon()
    {
        return $this->etageresRayon;
    }
}

<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Rayon
 */
class Rayon
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $designation;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rayonEtageres;

    /**
     * @var \ProjetBibliothequeBundle\Entity\Theme
     */
    private $rayonsTheme;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rayonEtageres = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return Rayon
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Add rayonEtagere
     *
     * @param \ProjetBibliothequeBundle\Entity\Etagere $rayonEtagere
     *
     * @return Rayon
     */
    public function addRayonEtagere(\ProjetBibliothequeBundle\Entity\Etagere $rayonEtagere)
    {
        $this->rayonEtageres[] = $rayonEtagere;

        return $this;
    }

    /**
     * Remove rayonEtagere
     *
     * @param \ProjetBibliothequeBundle\Entity\Etagere $rayonEtagere
     */
    public function removeRayonEtagere(\ProjetBibliothequeBundle\Entity\Etagere $rayonEtagere)
    {
        $this->rayonEtageres->removeElement($rayonEtagere);
    }

    /**
     * Get rayonEtageres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRayonEtageres()
    {
        return $this->rayonEtageres;
    }

    /**
     * Set rayonsTheme
     *
     * @param \ProjetBibliothequeBundle\Entity\Theme $rayonsTheme
     *
     * @return Rayon
     */
    public function setRayonsTheme(\ProjetBibliothequeBundle\Entity\Theme $rayonsTheme = null)
    {
        $this->rayonsTheme = $rayonsTheme;

        return $this;
    }

    /**
     * Get rayonsTheme
     *
     * @return \ProjetBibliothequeBundle\Entity\Theme
     */
    public function getRayonsTheme()
    {
        return $this->rayonsTheme;
    }
}

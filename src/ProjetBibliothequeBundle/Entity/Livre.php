<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Livre
 */
class Livre
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $titre;

    /**
     * @var string
     */
    private $notice;

    /**
     * @var integer
     */
    private $nbExemplaire;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $livreExemplaires;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $livresAuteurs;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $livresThemes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->livreExemplaires = new \Doctrine\Common\Collections\ArrayCollection();
        $this->livresAuteurs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->livresThemes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Livre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set notice
     *
     * @param string $notice
     *
     * @return Livre
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * Get notice
     *
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * Set nbExemplaire
     *
     * @param integer $nbExemplaire
     *
     * @return Livre
     */
    public function setNbExemplaire($nbExemplaire)
    {
        $this->nbExemplaire = $nbExemplaire;

        return $this;
    }

    /**
     * Get nbExemplaire
     *
     * @return integer
     */
    public function getNbExemplaire()
    {
        return $this->nbExemplaire;
    }

    /**
     * Add livreExemplaire
     *
     * @param \ProjetBibliothequeBundle\Entity\Exemplaire $livreExemplaire
     *
     * @return Livre
     */
    public function addLivreExemplaire(\ProjetBibliothequeBundle\Entity\Exemplaire $livreExemplaire)
    {
        $this->livreExemplaires[] = $livreExemplaire;

        return $this;
    }

    /**
     * Remove livreExemplaire
     *
     * @param \ProjetBibliothequeBundle\Entity\Exemplaire $livreExemplaire
     */
    public function removeLivreExemplaire(\ProjetBibliothequeBundle\Entity\Exemplaire $livreExemplaire)
    {
        $this->livreExemplaires->removeElement($livreExemplaire);
    }

    /**
     * Get livreExemplaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLivreExemplaires()
    {
        return $this->livreExemplaires;
    }

    /**
     * Add livresAuteur
     *
     * @param \ProjetBibliothequeBundle\Entity\Auteur $livresAuteur
     *
     * @return Livre
     */
    public function addLivresAuteur(\ProjetBibliothequeBundle\Entity\Auteur $livresAuteur)
    {
        $this->livresAuteurs[] = $livresAuteur;

        return $this;
    }

    /**
     * Remove livresAuteur
     *
     * @param \ProjetBibliothequeBundle\Entity\Auteur $livresAuteur
     */
    public function removeLivresAuteur(\ProjetBibliothequeBundle\Entity\Auteur $livresAuteur)
    {
        $this->livresAuteurs->removeElement($livresAuteur);
    }

    /**
     * Get livresAuteurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLivresAuteurs()
    {
        return $this->livresAuteurs;
    }

    /**
     * Add livresTheme
     *
     * @param \ProjetBibliothequeBundle\Entity\Theme $livresTheme
     *
     * @return Livre
     */
    public function addLivresTheme(\ProjetBibliothequeBundle\Entity\Theme $livresTheme)
    {
        $this->livresThemes[] = $livresTheme;

        return $this;
    }

    /**
     * Remove livresTheme
     *
     * @param \ProjetBibliothequeBundle\Entity\Theme $livresTheme
     */
    public function removeLivresTheme(\ProjetBibliothequeBundle\Entity\Theme $livresTheme)
    {
        $this->livresThemes->removeElement($livresTheme);
    }

    /**
     * Get livresThemes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLivresThemes()
    {
        return $this->livresThemes;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->titre;
    }
}

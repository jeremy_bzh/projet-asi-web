<?php

namespace ProjetBibliothequeBundle\Entity;

/**
 * Faculte
 */
class Faculte
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $faculteInscrit;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->faculteInscrit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Faculte
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add faculteInscrit
     *
     * @param \ProjetBibliothequeBundle\Entity\Inscrit $faculteInscrit
     *
     * @return Faculte
     */
    public function addFaculteInscrit(\ProjetBibliothequeBundle\Entity\Inscrit $faculteInscrit)
    {
        $this->faculteInscrit[] = $faculteInscrit;

        return $this;
    }

    /**
     * Remove faculteInscrit
     *
     * @param \ProjetBibliothequeBundle\Entity\Inscrit $faculteInscrit
     */
    public function removeFaculteInscrit(\ProjetBibliothequeBundle\Entity\Inscrit $faculteInscrit)
    {
        $this->faculteInscrit->removeElement($faculteInscrit);
    }

    /**
     * Get faculteInscrit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFaculteInscrit()
    {
        return $this->faculteInscrit;
    }
}


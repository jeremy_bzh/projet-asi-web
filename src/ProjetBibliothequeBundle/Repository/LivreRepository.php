<?php

namespace ProjetBibliothequeBundle\Repository;

/**
 * LivreRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LivreRepository extends \Doctrine\ORM\EntityRepository
{

    /*
    public function listerLivre(){
        $queryBuilder = $this->createQueryBuilder('qb');

        return $queryBuilder->select('qb.id', 'qb.titre')->getQuery()->getResult();
    }
    */

    public function listerLivres($titre){
        $queryBuilder = $this->createQueryBuilder('qb');
        $queryBuilder
            ->select('qb.id','qb.titre','qb.nbExemplaire')
            ->where('qb.titre LIKE :recherche')
            ->setParameter('recherche','%'.$titre.'%');
        return $queryBuilder->getQuery()->getResult();
    }
}

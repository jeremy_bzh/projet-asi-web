-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 30 Mars 2016 à 15:44
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bibliotheque`
--

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

CREATE TABLE IF NOT EXISTS `livre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nb_exemplaire` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=101 ;

--
-- Contenu de la table `livre`
--

INSERT INTO `livre` (`id`, `titre`, `notice`, `nb_exemplaire`) VALUES
(1, 'dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit', 'dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere, enim nisl elementum purus, accumsan interdum libero', 90),
(2, 'Praesent eu nulla at sem molestie sodales. Mauris blandit', 'pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit', 83),
(3, 'Aliquam auctor, velit eget laoreet posuere, enim nisl', 'mauris, aliquam eu, accumsan sed, facilisis', 83),
(4, 'vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor', 'Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere,', 82),
(5, 'hendrerit neque. In', 'imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo', 76),
(6, 'amet massa.', 'Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis', 97),
(7, 'amet,', 'Morbi neque tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor', 95),
(8, 'metus.', 'pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac', 74),
(9, 'Nullam suscipit, est ac facilisis facilisis, magna tellus', 'libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id', 85),
(10, 'tempor lorem, eget mollis lectus pede et risus. Quisque', 'gravida sit amet, dapibus id, blandit', 65),
(11, 'imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam', 'molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat', 65),
(12, 'ac metus vitae', 'amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem', 99),
(13, 'nec, diam. Duis', 'metus. Aenean sed', 55),
(14, 'lectus ante dictum mi, ac mattis', 'Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non,', 68),
(15, 'consectetuer, cursus', 'Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum', 63),
(16, 'dui,', 'Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis', 51),
(17, 'varius orci, in consequat enim diam', 'libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus', 66),
(18, 'lorem,', 'odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae', 78),
(19, 'laoreet', 'sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi', 92),
(20, 'Duis a mi fringilla mi lacinia', 'Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque', 89),
(21, 'vitae odio sagittis semper. Nam', 'orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis', 95),
(22, 'ut lacus. Nulla', 'mi', 50),
(23, 'elementum', 'Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum.', 95),
(24, 'sed, sapien. Nunc pulvinar arcu et', 'a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at', 73),
(25, 'dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales', 'Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat', 57),
(26, 'enim nisl elementum purus, accumsan interdum libero dui nec', 'ultricies adipiscing, enim mi tempor lorem, eget mollis lectus', 81),
(27, 'neque venenatis lacus.', 'aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing', 56),
(28, 'quam. Pellentesque habitant morbi tristique senectus et', 'mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam', 67),
(29, 'lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum', 'nisl sem,', 79),
(30, 'risus odio, auctor vitae, aliquet nec, imperdiet nec, leo.', 'dignissim. Maecenas ornare egestas ligula.', 52),
(31, 'Quisque purus', 'ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu turpis. Nulla', 66),
(32, 'pede sagittis augue, eu', 'tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus', 72),
(33, 'elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut', 'eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis,', 72),
(34, 'purus.', 'venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra. Donec', 90),
(35, 'metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper', 'Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec', 63),
(36, 'sagittis. Nullam vitae diam. Proin', 'Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna', 63),
(37, 'neque. Sed eget lacus. Mauris', 'pretium neque. Morbi quis urna. Nunc quis arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt congue turpis. In condimentum.', 97),
(38, 'commodo tincidunt', 'commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in,', 95),
(39, 'Duis elementum, dui quis accumsan convallis, ante lectus', 'cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit,', 71),
(40, 'Maecenas', 'Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis', 79),
(41, 'congue, elit sed consequat auctor, nunc nulla vulputate', 'adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor', 87),
(42, 'nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus', 'Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula.', 66),
(43, 'Sed id risus quis diam luctus lobortis. Class', 'ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis.', 74),
(44, 'dolor sit amet, consectetuer', 'convallis, ante lectus convallis est, vitae sodales', 54),
(45, 'faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus.', 'condimentum. Donec at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel pede', 81),
(46, 'ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing', 'non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim.', 94),
(47, 'lorem ipsum sodales purus, in molestie tortor', 'Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin', 85),
(48, 'tortor. Integer aliquam adipiscing lacus.', 'per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In', 58),
(49, 'interdum enim non nisi. Aenean eget metus. In', 'Sed molestie. Sed id risus quis diam luctus lobortis. Class', 88),
(50, 'Curae; Donec', 'gravida molestie arcu. Sed eu', 60),
(51, 'ac orci. Ut', 'justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis nec mauris blandit mattis. Cras', 98),
(52, 'luctus', 'enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus.', 98),
(53, 'pede, ultrices a, auctor non, feugiat', 'fermentum convallis ligula. Donec luctus aliquet odio. Etiam', 66),
(54, 'Etiam ligula tortor, dictum', 'odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante.', 90),
(55, 'neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin', 'consectetuer rhoncus. Nullam velit dui, semper et, lacinia', 92),
(56, 'luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae', 'semper tellus id', 55),
(57, 'nec, euismod in, dolor.', 'ipsum leo', 75),
(58, 'eu', 'velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin', 67),
(59, 'a, arcu. Sed et libero. Proin mi. Aliquam', 'cursus. Nunc mauris elit,', 61),
(60, 'metus eu', 'dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus', 93),
(61, 'tempor lorem,', 'ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque scelerisque', 63),
(62, 'semper', 'est ac mattis semper, dui lectus rutrum urna, nec luctus felis', 65),
(63, 'dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris,', 'interdum.', 82),
(64, 'tempor augue ac ipsum. Phasellus vitae mauris', 'enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor', 71),
(65, 'tristique neque venenatis', 'In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet', 86),
(66, 'urna justo faucibus lectus, a sollicitudin orci sem eget', 'ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed', 98),
(67, 'velit.', 'dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas', 64),
(68, 'vel', 'mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem', 67),
(69, 'facilisis, magna', 'lectus justo eu arcu. Morbi sit amet massa.', 79),
(70, 'varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas', 'a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum.', 100),
(71, 'nisi', 'iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus', 84),
(72, 'nulla ante, iaculis nec, eleifend non,', 'lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis dis parturient', 53),
(73, 'ac nulla. In', 'Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui.', 65),
(74, 'sem magna nec quam. Curabitur vel lectus. Cum', 'iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus.', 51),
(75, 'erat semper rutrum. Fusce dolor quam, elementum at,', 'egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec', 62),
(76, 'aliquet magna a neque.', 'erat semper rutrum. Fusce dolor quam, elementum at, egestas a,', 94),
(77, 'elit erat vitae', 'auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id', 52),
(78, 'nec ante. Maecenas mi', 'neque vitae semper egestas, urna', 56),
(79, 'metus eu erat semper rutrum. Fusce dolor quam, elementum at,', 'orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque', 89),
(80, 'egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla', 'non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu', 90),
(81, 'adipiscing elit. Aliquam auctor, velit', 'eget metus. In nec orci. Donec', 84),
(82, 'metus sit amet ante. Vivamus non lorem', 'tellus id nunc', 66),
(83, 'Sed eu eros. Nam consequat', 'ac arcu. Nunc', 57),
(84, 'aliquet. Proin velit. Sed malesuada augue ut', 'Nunc sollicitudin', 81),
(85, 'a tortor. Nunc commodo', 'vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id', 99),
(86, 'turpis. Nulla aliquet.', 'mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare', 93),
(87, 'consectetuer mauris id sapien. Cras dolor dolor, tempus', 'amet luctus vulputate, nisi sem semper', 51),
(88, 'porttitor tellus non magna. Nam ligula elit, pretium et, rutrum', 'Aenean gravida nunc sed pede. Cum sociis', 68),
(89, 'Vivamus molestie dapibus ligula. Aliquam erat', 'et,', 84),
(90, 'elit, a feugiat tellus lorem eu metus. In lorem. Donec', 'nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient', 98),
(91, 'elementum, lorem ut aliquam iaculis,', 'pellentesque a, facilisis', 82),
(92, 'Cras eu tellus eu augue porttitor interdum.', 'magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur', 82),
(93, 'Mauris quis', 'lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus', 74),
(94, 'In scelerisque scelerisque dui. Suspendisse ac metus vitae velit', 'ac nulla. In tincidunt congue turpis. In', 76),
(95, 'turpis non enim. Mauris quis turpis vitae purus gravida sagittis.', 'ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas', 66),
(96, 'ridiculus', 'lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et', 95),
(97, 'eget nisi dictum augue malesuada', 'cursus in, hendrerit', 94),
(98, 'egestas ligula. Nullam feugiat placerat velit.', 'Praesent interdum ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus', 63),
(99, 'mi lacinia mattis. Integer eu lacus.', 'vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis', 57),
(100, 'sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla', 'vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan', 58);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
